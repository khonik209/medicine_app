$(document).ready(function () {
    /* Объявляем переменные */
    var user = null;
    var illnesses = {};
    var records = {};
    /* Запрашиваем информацию для страницы */
    $.post('http://teledoctor-demo.local/api/home', {token: 'AAaa1234'}, function (res) {
        console.log(res);
        if (res.code != 0) {
            alert("Ошибка в запросе")
        } else {
            /* Сохраняем переменные */
            user = res.object.user;
            illnesses = res.object.illnesses;
            records = res.object.records;
            /* Расставляем данные по своим местам */
            $('#user_name').text(user.name);
            if (illnesses.length > 0) {
                for (var i = 0; i < illnesses.length; i++) {
                    $("#illnesses").append("<li><a href='#'>" + illnesses[i].name + "</a></li>")
                }
            } else {
                $("#illnesses").append("<li>Обращения отсутствуют</li>");
            }
            if (records.length > 0) {
                for (var j = 0; j < records.length; j++) {
                    $("#records").append("<li><a href='#'>" + records[j].name + "</a></li>");
                }
            } else {
                $("#records").append("<li>Записи отсутствуют</li>");
            }
        }
    });
    /* При переключении страницы-"вкладки" */
    $(document).bind("pagechange", function (event, obj) {
        var currentPage = obj.toPage[0].id;

        $('.btn-footer').removeClass('ui-btn-active');
        $('#'+currentPage).find('[href="#' + currentPage + '"]').addClass('ui-btn-active');

        /*if (obj.prevPage) {
            var prev = obj.prevPage[0].id;
            if (prev === currentPage) {
                return false;
            }
        }*/

        /* Обновляем соотв. данные */
        if (currentPage === "illnesses-page") {
            $("#illnesses").listview("refresh");
        } else if (currentPage === "records-page") {

            $("#records").listview("refresh");
        }
    });
});